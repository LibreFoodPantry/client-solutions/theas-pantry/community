import globals from "globals";
import pluginJs from "@eslint/js";
import eslintPluginJsonc from 'eslint-plugin-jsonc';


export default [
  {
    env: {
      es2021: true,
      node: true
    },

    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.node
      } 
    }
  },

  pluginJs.configs.recommended,
  ...eslintPluginJsonc.configs['flat/recommended-with-json']
];
